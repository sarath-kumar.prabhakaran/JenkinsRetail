package com.retail.core;

//Air Class implements the Shipping Interface.
public class Air implements Shipping{

	@Override
	public double shippingCost(double weight, String upc) {
		int code = upc.charAt(upc.length() - 2)-'0';
		
		return weight*code;
	}

}
