package com.retail.core;

// Strategy Pattern
// Based on the Shipping Method Respective Class methods will be called.
public class Strategy {
	   private Shipping strategy;

	   public Strategy(Shipping strategy){
	      this.strategy = strategy;
	   }

	   public double executeStrategy(double weight, String upc){
	      return strategy.shippingCost(weight, upc);
	   }
	}
