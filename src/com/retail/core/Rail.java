package com.retail.core;

public class Rail implements Shipping{
	
	@Override
	public double shippingCost(double weight, String upc) {
		
		if(weight<=5) {
			return 5.0;
		}else {
			return 10.0;
		}
	}

}
