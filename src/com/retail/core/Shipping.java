package com.retail.core;

public interface Shipping {
	
	 public double shippingCost(double weight, String upc);
	
}
