package com.retail.core;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Item {
	
	//Item Class Details
	private String description; 
	private double price; 
	private double weight; 
	private String shippingMethod;
	private String upc;
	private double shippingCost;
	
	//Parameterized constructor
	public Item(String desc, double cost, double wt, String shipMeth,String upcn) {
		setDescription(desc);
		setPrice(cost);
		setShippingMethod(shipMeth);
		setWeight(wt);
		setUpc(upcn);
	}
	
	// Getters and Setters
	
	public String getUpc() {
		return upc;
	}

	public void setUpc(String upc) {
		this.upc = upc;
	}

	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public double getPrice() {
		return price;
	}


	public void setPrice(double price) {
		this.price = price;
	}


	public double getWeight() {
		return weight;
	}


	public void setWeight(double weight) {
		this.weight = weight;
	}

	public String getShippingMethod() {
		return shippingMethod;
	}


	public void setShippingMethod(String shippingMethod) {
		this.shippingMethod = shippingMethod;
	}
	
	public double getShippingCost() {
		return shippingCost;
	}

	public void setShippingCost(double shippingCost) {
		this.shippingCost = shippingCost;
	}
	
	
	//Factory Pattern - based on the Shipping Method new object is Getting Created for Respective Class
	public Shipping getshipMethod(String shipmethod){
			
	      if(shipmethod.equals("AIR")){
	         return new Air();
	         
	      } else if(shipmethod.equals("RAIL")){
	    	  return new Rail();	  
	    	  }
	      else {
	         return new Ground();
	      }
	}
	
	@Override
	public String toString() {
		return "description=" + description + ", price=" + price + ", weight=" + weight + ", shippingMethod="
				+ shippingMethod + ", upc=" + upc + ", shippingCost=" + shippingCost;
	}
}