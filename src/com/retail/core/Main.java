package com.retail.core;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Main {
	
	public static void main(String[] args) {
		
		Strategy stat;
		double sum = 0.0;
		
		List<Item> Item_list = new ArrayList<Item>(){
            {
                add(new Item( "CD � Pink Floyd, Side Of Moond",19.99, 0.58, "AIR", "567321101987"));
                add(new Item( "CD � Beatles, Abbey Road",17.99, 0.61, "GROUND", "567321101986"));
                add(new Item( "CD � Queen, A Night at Opera",20.49, 0.55, "AIR", "567321101985"));
                add(new Item( "CD � Michael Jackson, Thriller",23.88, 0.50, "GROUND", "567321101984"));
                add(new Item( "iPhone - Waterproof Case",9.75, 0.73, "AIR", "467321101899"));
                add(new Item( "iPhone -  Headphones     ",17.25, 3.21, "GROUND", "477321101878"));
            }

      };
		
		System.out.print("********SHIPMENT REPORT****");
		
		System.out.print("\t\t\t\t\t\t\t");
		System.out.println("Date:"+ LocalDateTime.now());
		
		System.out.println();
		
		System.out.print("UPC");
		System.out.print("\t\t");
		System.out.print("Description");
		System.out.print("\t\t\t\t");
		System.out.print("Price");
		System.out.print("\t\t");
		System.out.print("Weight");
		System.out.print("\t\t");
		System.out.print("Ship Method");
		System.out.print("\t");
		System.out.println("Shipping Cost");
		
		System.out.println();
		
		
		//TreeMap for storing the Items data 
		// key - UPC
		// Value - [description,price,weight,shippingMethod,shippingCost]
		Map<String, ArrayList<String>> hm = new TreeMap<String, ArrayList<String>>();
		
		//Calculations for Shipping Cost
		for(Item i : Item_list) {
			stat = new Strategy(i.getshipMethod(i.getShippingMethod()));
			i.setShippingCost(stat.executeStrategy(i.getWeight(), i.getUpc()));
			sum += i.getShippingCost();
		}
		
		
		// HashMap Creation.
		for(Item i : Item_list) {
		ArrayList<String> l = new ArrayList<String>();
		l.add(i.getDescription());
		l.add(String.valueOf(i.getPrice()));
		l.add(String.valueOf(i.getWeight()));
		l.add(i.getShippingMethod());
		l.add(String.valueOf(i.getShippingCost()));
		hm.put(i.getUpc(), l);
		}
		
		// Display as a Report
		for (String k : hm.keySet()) {
			System.out.print(k);
			List<String> ls = hm.get(k);
			System.out.print("\t");
			for(String i:ls) {
				System.out.print(i);
				System.out.print("\t\t");
			}
			System.out.println();
		}
		
		System.out.println();
		System.out.println("TOTAL SHIPPING COST: \t\t\t\t\t\t\t\t\t\t\t"+ sum);
				
	}

}
