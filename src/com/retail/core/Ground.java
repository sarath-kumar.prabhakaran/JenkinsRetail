package com.retail.core;

//Ground Class implements the Shipping Interface.
public class Ground implements Shipping{

	@Override
	public double shippingCost(double weight, String upc) {
		
		double cost = weight*2.5;
	
		return Math.round(cost*100.0)/100.0;
	}

}
