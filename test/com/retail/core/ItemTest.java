package com.retail.core;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ItemTest {
	
	private Item new_item_air;
	private Item new_item_ground;
	private Item new_item_rail1;
	private Item new_item_rail2;
	
	@Before
	public void itemInit() {
		new_item_air = new Item("CD � Pink Floyd, Side Of Moond",19.99, 0.58, "AIR", "567321101987");	
		new_item_ground = new Item("CD � Beatles, Abbey Road",17.99, 0.61, "GROUND", "567321101986");
		new_item_rail1 = new Item("Hot Tub",9899.99, 793.41, "RAIL", "312321101516");
		new_item_rail2 = new Item("HeavyMac Laptop",4555.79, 4.08, "RAIL", "322322202488");
	}

	@Test
	public void testAir() {
		new_item_air.setShippingCost(new Air().shippingCost(new_item_air.getWeight(), new_item_air.getUpc()));
		double actual = new_item_air.getShippingCost();
		double expected = 4.64;
		assertEquals(expected, actual,0.001);
	}
	
	@Test
	public void testAirfail() {
		new_item_air.setShippingCost(new Air().shippingCost(new_item_air.getWeight(), new_item_air.getUpc()));
		double actual = new_item_air.getShippingCost();
		double expected = 4.22;
		assertNotEquals(expected, actual,0.001);
	}
	
	@Test
	public void testGround() {
		new_item_ground.setShippingCost(new Ground().shippingCost(new_item_ground.getWeight(), new_item_ground.getUpc()));
		double actual = new_item_ground.getShippingCost();
		double expected = 1.53;
		assertEquals(expected, actual,0.001);
	}
	
	@Test
	public void testGroundfail() {
		new_item_ground.setShippingCost(new Ground().shippingCost(new_item_ground.getWeight(), new_item_ground.getUpc()));
		double actual = new_item_ground.getShippingCost();
		double expected = 1.23;
		assertNotEquals(expected, actual,0.001);
	}
	
	@Test
	public void testRail1() {
		new_item_rail1.setShippingCost(new Rail().shippingCost(new_item_rail1.getWeight(), new_item_rail1.getUpc()));
		double actual = new_item_rail1.getShippingCost();
		double expected = 10.0;
		assertEquals(expected, actual,0.001);
	}
	
	@Test
	public void testRailfail1() {
		new_item_rail2.setShippingCost(new Rail().shippingCost(new_item_rail2.getWeight(), new_item_rail2.getUpc()));
		double actual = new_item_rail2.getShippingCost();
		double expected = 1.23;
		assertNotEquals(expected, actual,0.001);
	}
	
	@Test
	public void testRail2() {
		new_item_rail2.setShippingCost(new Rail().shippingCost(new_item_rail2.getWeight(), new_item_rail2.getUpc()));
		double actual = new_item_rail2.getShippingCost();
		double expected = 5.0;
		assertEquals(expected, actual,0.001);
	}
	
	@Test
	public void testRailfail2() {
		new_item_rail2.setShippingCost(new Rail().shippingCost(new_item_rail2.getWeight(), new_item_rail2.getUpc()));
		double actual = new_item_rail2.getShippingCost();
		double expected = 1.23;
		assertNotEquals(expected, actual,0.001);
	}
	
	@After
	public void destroy() {
		new_item_air = null;
		new_item_ground = null;
	}
}
